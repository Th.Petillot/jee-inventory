INSERT INTO user (password, email, enabled, last_password_reset_date) VALUES ('$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi', 'admin@admin.com', 1, STR_TO_DATE('01-01-2016', '%d-%m-%Y'));
INSERT INTO user (password, email, enabled, last_password_reset_date) VALUES ('$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', 'enabled@user.com', 1, STR_TO_DATE('01-01-2016', '%d-%m-%Y'));
INSERT INTO user (password, email, enabled, last_password_reset_date) VALUES ('$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', 'disabled@user.com', 0, STR_TO_DATE('01-01-2016', '%d-%m-%Y'));

INSERT INTO authority (name) VALUES ('ROLE_USER');
INSERT INTO authority (name) VALUES ('ROLE_ADMIN');

INSERT INTO user_authority (user_id, authority_id) VALUES (1, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (1, 2);
INSERT INTO user_authority (user_id, authority_id) VALUES (2, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (3, 1);

INSERT INTO product (id, name) VALUES (1, 'Coca-Cola');
INSERT INTO product (id, name) VALUES (2, 'Pizza 4 fromages');
INSERT INTO product (id, name) VALUES (3, 'Baguettine');
INSERT INTO product (id, name) VALUES (4, 'Pates');
INSERT INTO product (id, name) VALUES (5 ,'Chips');

INSERT INTO inventory (id, creation_date, close_date, state) VALUES (1, '2011-03-11', null, 'PLANNING');
INSERT INTO inventory (id, creation_date, close_date, state) VALUES (2, '2011-03-11', null, 'DURING');
INSERT INTO inventory (id, creation_date, close_date, state) VALUES (3, '2011-03-11', '2011-03-11', 'CLOSED');

INSERT INTO inventory_product (id, quantity, inventory_id, product_id) VALUES (1, 5, 1, 3);
INSERT INTO inventory_product (id, quantity, inventory_id, product_id) VALUES (2, 5, 1, 2);
INSERT INTO inventory_product (id, quantity, inventory_id, product_id) VALUES (3, 5, 1, 1);
