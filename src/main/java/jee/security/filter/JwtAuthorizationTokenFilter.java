package jee.security.filter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import jee.security.util.JwtToken;
import jee.security.util.JwtTokenInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthorizationTokenFilter extends OncePerRequestFilter {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final UserDetailsService userDetailsService;
    private final String tokenHeader;
    private final JwtTokenInitializer jwtTokenInitializer;

    public JwtAuthorizationTokenFilter(JwtTokenInitializer jwtTokenInitializer,
                                       @Value("${jwt.header}") String tokenHeader,
                                       @Qualifier("jwtUserDetailsService")
                                               UserDetailsService userDetailsService) {

        this.userDetailsService = userDetailsService;
        this.tokenHeader = tokenHeader;
        this.jwtTokenInitializer = jwtTokenInitializer;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain chain) throws ServletException, IOException {
        final String requestHeader = request.getHeader(this.tokenHeader);

        if (requestHeader != null
                && requestHeader.startsWith("Bearer ")
                && requestHeader.length() > 7) {
            String token = requestHeader.substring(7);
            try {
                JwtToken jwtToken = jwtTokenInitializer.createJwtToken(token);
                performAuthenticationFromJwtToken(jwtToken, request);
            } catch (IllegalArgumentException e) {
                logger.error("an error occured during getting username from token", e);
            } catch (ExpiredJwtException e) {
                logger.warn("the token is expired and not valid anymore", e);
            }
        } else {
            logger.warn("couldn't find bearer string, will ignore the header");
        }

        chain.doFilter(request, response);
    }

    private void performAuthenticationFromJwtToken(JwtToken jwtToken, HttpServletRequest request) {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            Claims claims = jwtToken.getClaims();
            UserDetails userDetails = userDetailsService.loadUserByUsername(claims.getSubject());

            if (jwtToken.validate(userDetails)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
    }
}
