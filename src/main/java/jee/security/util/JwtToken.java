package jee.security.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.DefaultClock;
import jee.security.JwtUser;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;

public class JwtToken {
    private Clock clock = DefaultClock.INSTANCE;

    private String secret;
    private String token;

    private Claims claims;

    public JwtToken(String token, String secret) {
        this.token = token;
        this.secret = secret;
    }

    public String getToken() {
        return token;
    }

    public Claims getClaims() {
        if (!hasClaims()) {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        }

        return claims;
    }

    public Boolean canTokenBeRefreshed(Date lastPasswordReset) {
        final Date created = getClaims().getIssuedAt();
        return !isCreatedBeforeLastPasswordReset(created, lastPasswordReset)
                && !isTokenExpired();
    }

    public Boolean validate(UserDetails userDetails) {
        JwtUser user = (JwtUser) userDetails;
        Claims claims = getClaims();
        final String username = claims.getSubject();
        final Date created = claims.getIssuedAt();
        return (
                username.equals(user.getUsername())
                        && !isTokenExpired()
                        && !isCreatedBeforeLastPasswordReset(created, user.getLastPasswordResetDate())
        );
    }

    private Boolean isTokenExpired() {
        final Date expiration = getClaims().getExpiration();
        return expiration.before(clock.now());
    }

    private Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordReset) {
        return (lastPasswordReset != null && created.before(lastPasswordReset));
    }

    private boolean hasClaims() {
        return claims != null;
    }
}
