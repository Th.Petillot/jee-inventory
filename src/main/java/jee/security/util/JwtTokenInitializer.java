package jee.security.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JwtTokenInitializer {
    @Value("${jwt.secret}")
    private String secret;

    public JwtToken createJwtToken(String token) {
        return new JwtToken(token, secret);
    }
}
