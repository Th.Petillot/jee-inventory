package jee.security.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtTokenFactory {
    private Clock clock = DefaultClock.INSTANCE;

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    private JwtTokenUtil jwtTokenUtil;

    public JwtTokenFactory(JwtTokenUtil jwtTokenUtil) {
        this.jwtTokenUtil = jwtTokenUtil;
    }

    public String createToken(Claims claims, String subject) {
        final Date createdDate = clock.now();
        final Date expirationDate =
                jwtTokenUtil.calculateExpirationDate(createdDate);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public Claims refreshClaims(JwtToken token) {
        final Date createdDate = clock.now();
        final Date expirationDate =
                jwtTokenUtil.calculateExpirationDate(createdDate);

        final Claims claims = token.getClaims();

        claims.setIssuedAt(createdDate);
        claims.setExpiration(expirationDate);

        return claims;
    }

    public String refreshToken(Claims claims) {
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
}
