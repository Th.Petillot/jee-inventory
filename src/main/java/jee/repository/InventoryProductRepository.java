package jee.repository;

import jee.entity.InventoryProduct;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InventoryProductRepository extends CrudRepository<InventoryProduct, Long> {

    InventoryProduct findByInventoryIdAndProductId(Long inventoryId, Long productId);
    List<InventoryProduct> findByInventoryId(Long inventoryId);

}
