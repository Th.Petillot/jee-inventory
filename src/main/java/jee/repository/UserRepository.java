package jee.repository;

import jee.entity.security.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface
UserRepository extends JpaRepository<User, Long> {
    User findUserByEmail(String email);
}
