package jee.repository;

import jee.entity.security.Authority;
import jee.entity.security.AuthorityName;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AuthorityRepository extends CrudRepository<Authority, Long> {
    Authority findAuthorityByName(AuthorityName name);
    List<Authority> findAllByNameIn(List<AuthorityName> names);
}
