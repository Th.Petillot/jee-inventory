package jee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }

    @Bean
    public CorsFilter corsFilter() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration conf = new CorsConfiguration();
        conf.setAllowCredentials(true);
        conf.addAllowedHeader("*");
        conf.addAllowedOrigin("*");
        conf.addAllowedMethod("OPTIONS");
        conf.addAllowedMethod("POST");
        conf.addAllowedMethod("GET");
        conf.addAllowedMethod("PUT");
        conf.addAllowedMethod("DELETE");
        source.registerCorsConfiguration("/**", conf);
        return new CorsFilter(source);
    }
}
