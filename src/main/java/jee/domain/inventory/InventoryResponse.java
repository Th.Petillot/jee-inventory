package jee.domain.inventory;

import jee.entity.Inventory;
import jee.entity.security.User;
import jee.entity.util.State;

import java.util.Date;

public class InventoryResponse {

    private Long id;
    private State state;
    private Date creationDate;
    private Date closeDate;


    public InventoryResponse() {
    }

    public InventoryResponse(Inventory inventory) {
        id = inventory.getId();
        state = inventory.getState();
        creationDate = inventory.getCreationDate();
        closeDate = inventory.getCloseDate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }
}
