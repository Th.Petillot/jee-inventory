package jee.domain.inventory;

import jee.entity.Inventory;
import jee.entity.security.User;
import jee.entity.util.State;

import java.util.Date;

public class InventoryRequest {

    private State state;
    private Date creationDate;
    private Date closeDate;


    public InventoryRequest() {
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public Inventory build() {
        Inventory result = new Inventory();
        result.setState(state);
        result.setCreationDate(creationDate);
        result.setCloseDate(closeDate);
        return result;
    }
}
