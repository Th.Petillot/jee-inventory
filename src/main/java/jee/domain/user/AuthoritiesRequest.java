package jee.domain.user;

import jee.entity.security.AuthorityName;

import javax.validation.constraints.NotNull;
import java.util.List;

public class AuthoritiesRequest {
    @NotNull
    private List<AuthorityName> authoritiesNames;

    public List<AuthorityName> getAuthoritiesNames() {
        return authoritiesNames;
    }

    public void setAuthoritiesNames(List<AuthorityName> authoritiesNames) {
        this.authoritiesNames = authoritiesNames;
    }
}
