package jee.domain.user;

import jee.domain.validator.ValidPassword;
import jee.entity.security.User;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.ArrayList;

public class UserRequest {

    @NotNull
    @Email
    @Size(min = 8, max = 100)
    private String email;

    @NotNull
    @ValidPassword
    private String password;

    @NotNull
    private Boolean enabled;

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date lastPasswordResetDate;

    public UserRequest() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public void setLastPasswordResetDate(Date lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    public User build() {
        User result = new User();
        result.setEmail(email);
        result.setPassword(password);
        result.setLastPasswordResetDate(lastPasswordResetDate);
        result.setAuthorities(new ArrayList<>());
        result.setEnabled(true);
        return result;
    }
}
