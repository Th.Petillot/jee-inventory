package jee.domain.user;

import jee.entity.security.Authority;
import jee.entity.security.AuthorityName;
import jee.entity.security.User;

import java.util.List;
import java.util.stream.Collectors;

public class UserResponse {
    private Long id;
    private String email;
    private List<AuthorityName> authorities;

    public UserResponse() {
    }

    public UserResponse(User user) {
        id = user.getId();
        email = user.getEmail();

        authorities = user.getAuthorities().stream()
                .map(Authority::getName)
                .collect(Collectors.toList());

        id = user.getId();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<AuthorityName> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<AuthorityName> authorities) {
        this.authorities = authorities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
