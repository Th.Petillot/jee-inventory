package jee.domain.authentification;

import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class JwtAuthenticationResponse {
    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;

    private final List<String> authorities;

    private final Date expiration;

    public JwtAuthenticationResponse(String token,
                                     Collection<? extends GrantedAuthority> authorities, Date expiration) {

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>(authorities);
        this.authorities = grantedAuthorities
                .stream().map(Object::toString).collect(Collectors.toList());
        this.token = token;
        this.expiration = expiration;
    }

    public String getToken() {
        return this.token;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public Date getExpiration() {
        return expiration;
    }
}
