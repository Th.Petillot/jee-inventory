package jee.domain.product;

import jee.entity.Product;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ProductRequest {

    @NotNull
    @Size(min=2, max=100)
    private String name;

    public ProductRequest() {
    }

    public ProductRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Product build() {
        Product result = new Product();
        result.setName(name);
        return result;
    }
}
