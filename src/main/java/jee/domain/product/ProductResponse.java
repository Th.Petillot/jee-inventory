package jee.domain.product;

import jee.entity.Product;

public class ProductResponse {

    private Long id;
    private String name;

    public ProductResponse(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public ProductResponse() {
    }

    public ProductResponse(Product product) {
        id = product.getId();
        name = product.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
