package jee.domain.product;

public class ProductQuantityRequest {

    private Long productId;
    private int quantity;

    public ProductQuantityRequest() {
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
