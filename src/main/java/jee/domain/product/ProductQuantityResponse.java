package jee.domain.product;

import jee.entity.InventoryProduct;

public class ProductQuantityResponse {

    private Long inventoryId;
    private Long productId;
    private int quantity;

    public ProductQuantityResponse() {
    }

    public ProductQuantityResponse(InventoryProduct inventoryProduct) {
        inventoryId = inventoryProduct.getInventory().getId();
        productId = inventoryProduct.getProduct().getId();
        quantity = inventoryProduct.getQuantity();
    }

    public Long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
