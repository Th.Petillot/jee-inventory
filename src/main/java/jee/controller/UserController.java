package jee.controller;

import jee.domain.user.AuthoritiesRequest;
import jee.domain.user.UserRequest;
import jee.domain.user.UserResponse;
import jee.entity.security.Authority;
import jee.entity.security.AuthorityName;
import jee.entity.security.User;
import jee.repository.AuthorityRepository;
import jee.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping(path = "/user")
@PreAuthorize("hasRole('ADMIN')")
public class UserController {
    private final AuthorityName defaultAuthorityName = AuthorityName.ROLE_USER;
    private final UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private final AuthorityRepository authorityRepository;

    public UserController(UserRepository userRepository,
                          PasswordEncoder passwordEncoder,
                          AuthorityRepository authorityRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
    }

    @PostMapping(path = "")
    public @ResponseBody ResponseEntity createUser (@Valid @RequestBody UserRequest userRequest) {
        userRequest.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        User user = userRequest.build();

        List<Authority> authorities = new ArrayList<>();
        authorities.add(authorityRepository.findAuthorityByName(defaultAuthorityName));
        user.setAuthorities(authorities);

        userRepository.save(user);

        UserResponse response = new UserResponse(user);
        return ResponseEntity.ok(response);
    }

    @GetMapping(path="/all")
    public @ResponseBody ResponseEntity getAllUser() {
        List<UserResponse> response = new ArrayList<>();
        userRepository.findAll().forEach(user -> response.add(new UserResponse(user)));
        return ResponseEntity.ok(response);
    }

    @GetMapping(path="/{id}")
    public @ResponseBody ResponseEntity getUser(@PathVariable Long id) {
        User user = userRepository.findById(id).get();
        UserResponse response = new UserResponse(user);
        return ResponseEntity.ok(response);
    }

    @PostMapping(path="/{id}/role")
    public @ResponseBody ResponseEntity setAuthorities(
            @PathVariable Long id,
            @Valid @RequestBody AuthoritiesRequest authorityNames) {
        User user = userRepository.findById(id).get();

        List<Authority> authorities =
                authorityRepository.findAllByNameIn(authorityNames.getAuthoritiesNames());

        user.setAuthorities(authorities);

        userRepository.save(user);
        return ResponseEntity.ok(new UserResponse(user));
    }

    @ExceptionHandler(NoSuchElementException.class)
    private  @ResponseBody ResponseEntity noSuchElementExceptionHandler() {
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

}
