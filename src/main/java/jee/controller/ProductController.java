package jee.controller;


import jee.domain.product.ProductRequest;
import jee.domain.product.ProductResponse;
import jee.entity.Product;
import jee.entity.util.Updater;
import jee.repository.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.xml.ws.Response;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping(path = "/all")
    public @ResponseBody ResponseEntity getInventories() {
        List<ProductResponse> response = new ArrayList<>();
        productRepository.findAll()
                .forEach(product -> response.add(new ProductResponse(product)));
        return ResponseEntity.ok(response);
    }

    /**
     * Get
     * @param id
     * @return
     */
    @GetMapping(path = "/{id}")
    public @ResponseBody ResponseEntity readProduct(@PathVariable Long id) {
        Product product = productRepository.findById(id).get();
        return ResponseEntity.ok(product);
    }

    /**
     * Post
     * @param request
     * @return
     */
    @PostMapping(path = "")
    public @ResponseBody ResponseEntity createProduct(@Valid @RequestBody ProductRequest request) {
        Product product = request.build();
        productRepository.save(product);
        ProductResponse response = new ProductResponse(product.getId(), product.getName());
        return ResponseEntity.ok(response);
    }

    /**
     * Put
     * @param id
     * @param request
     * @return
     */
    @PutMapping(path = "/{id}")
    public @ResponseBody ResponseEntity updateProduct(@PathVariable Long id, @Valid @RequestBody ProductRequest request) {
        Product product = productRepository.findById(id).get();
        Updater.updateEntity(product, request);
        productRepository.save(product);
        ProductResponse response = new ProductResponse(product);
        return ResponseEntity.ok(response);
    }


    /**
     * Delete
     * @param id
     * @return
     */
    @DeleteMapping(path = "/{id}")
    public @ResponseBody ResponseEntity deleteProduct(@PathVariable Long id) {
        Product product = productRepository.findById(id).get();
        try {
            productRepository.delete(product);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("This product can't be delete. It's used in one or more inventory");
        }
        return ResponseEntity.ok(product);
    }


    @ExceptionHandler(NoSuchElementException.class)
    private  @ResponseBody ResponseEntity noSuchElementExceptionHandler() {
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }


}
