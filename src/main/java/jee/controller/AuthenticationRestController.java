package jee.controller;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import jee.security.util.JwtToken;
import jee.security.util.JwtTokenFactory;
import jee.security.util.JwtTokenInitializer;
import jee.security.JwtUser;
import jee.controller.exception.authentication.AuthenticationException;
import jee.domain.authentification.JwtAuthenticationRequest;
import jee.domain.authentification.JwtAuthenticationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestController
public class AuthenticationRestController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${jwt.header}")
    private String tokenHeader;

    private AuthenticationManager authenticationManager;
    private JwtTokenFactory jwtTokenFactory;
    private UserDetailsService userDetailsService;
    private final JwtTokenInitializer jwtTokenInitializer;

    public AuthenticationRestController(
            JwtTokenFactory jwtTokenFactory,
            JwtTokenInitializer jwtTokenInitializer) {

        this.jwtTokenFactory = jwtTokenFactory;
        this.jwtTokenInitializer = jwtTokenInitializer;
    }

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Autowired
    @Qualifier("jwtUserDetailsService")
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @PostMapping(path = "/auth")
    public ResponseEntity<?> createAuthenticationToken(
            @RequestBody JwtAuthenticationRequest authenticationRequest)
            throws AuthenticationException {

        try {
            authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        } catch (NullPointerException e) {
            throw new AuthenticationException("Bad credentials inputs", e);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        final String subject = userDetails.getUsername();
        final Claims claims = new DefaultClaims();
        final String token = jwtTokenFactory.createToken(claims, subject);

        return ResponseEntity.ok(new JwtAuthenticationResponse(token, userDetails.getAuthorities(), claims.getExpiration()));
    }

    @GetMapping(path = "/refresh")
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {

        String authToken = request.getHeader(tokenHeader);
        final String token = authToken.substring(7);

        JwtToken jwtToken = jwtTokenInitializer.createJwtToken(token);

        Claims claims = jwtToken.getClaims();
        JwtUser user =
                (JwtUser) userDetailsService.loadUserByUsername(claims.getSubject());

        if (jwtToken.canTokenBeRefreshed(user.getLastPasswordResetDate())) {
            Claims refreshClaims = jwtTokenFactory.refreshClaims(jwtToken);
            String refreshToken = jwtTokenFactory.refreshToken(refreshClaims);
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshToken, user.getAuthorities(), claims.getExpiration()));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

    /**
     * Authenticates the user. If something is wrong, an {@link AuthenticationException} will be thrown
     */
    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthenticationException("User is disabled!", e);
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("Bad credentials!", e);
        }
    }

}
