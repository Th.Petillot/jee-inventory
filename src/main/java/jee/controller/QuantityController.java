package jee.controller;

import jee.domain.product.ProductQuantityRequest;
import jee.domain.product.ProductQuantityResponse;
import jee.entity.Inventory;
import jee.entity.InventoryProduct;
import jee.entity.Product;
import jee.entity.util.State;
import jee.repository.InventoryProductRepository;
import jee.repository.InventoryRepository;
import jee.repository.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping(path = "/quantity")
public class QuantityController {

    private final InventoryProductRepository inventoryProductRepository;
    private final ProductRepository productRepository;
    private final InventoryRepository inventoryRepository;

    private QuantityController(InventoryProductRepository inventoryProductRepository,
                               ProductRepository productRepository,
                               InventoryRepository inventoryRepository) {
        this.inventoryProductRepository = inventoryProductRepository;
        this.productRepository = productRepository;
        this.inventoryRepository = inventoryRepository;
    }

    @PutMapping(path = "/add/{id}")
    public @ResponseBody ResponseEntity add(@PathVariable Long id, @RequestBody ProductQuantityRequest request) {
        Inventory inventory = inventoryRepository.findById(id).get();
        Product product = productRepository.findById(request.getProductId()).get();

        if (inventory.getState() == State.CLOSED) {
            return ResponseEntity.badRequest().body("Inventory is closed");
        }

        InventoryProduct invProd = inventoryProductRepository
                .findByInventoryIdAndProductId(inventory.getId(), product.getId());
        if (invProd == null) {
            invProd = new InventoryProduct();
            invProd.setProduct(product);
            invProd.setInventory(inventory);
        }

        invProd.setQuantity(invProd.getQuantity() + request.getQuantity());
        inventoryProductRepository.save(invProd);
        ProductQuantityResponse response = new ProductQuantityResponse(invProd);
        return ResponseEntity.ok(response);
    }

    @PutMapping(path = "/remove/{id}")
    public @ResponseBody ResponseEntity remove(@PathVariable Long id, @RequestBody ProductQuantityRequest request) {
        Inventory inventory = inventoryRepository.findById(id).get();
        Product product = productRepository.findById(request.getProductId()).get();

        if (inventory.getState() == State.CLOSED) {
            return ResponseEntity.badRequest().body("Inventory is closed");
        }

        InventoryProduct invProd = inventoryProductRepository
                .findByInventoryIdAndProductId(inventory.getId(), product.getId());
        if (invProd == null) {
            invProd = new InventoryProduct();
        }
        invProd.setQuantity(Math.max(invProd.getQuantity() - request.getQuantity(), 0));

        inventoryProductRepository.save(invProd);
        ProductQuantityResponse response = new ProductQuantityResponse(invProd);
        return ResponseEntity.ok(response);
    }

    @PutMapping(path = "/{id}")
    public @ResponseBody ResponseEntity set(@PathVariable Long id, @RequestBody ProductQuantityRequest request) {
        Inventory inventory = inventoryRepository.findById(id).get();
        Product product = productRepository.findById(request.getProductId()).get();

        if (inventory.getState() == State.CLOSED) {
            return ResponseEntity.badRequest().body("Inventory is closed");
        }

        InventoryProduct invProd = inventoryProductRepository
                .findByInventoryIdAndProductId(inventory.getId(), product.getId());
        if (invProd == null) {
            invProd = new InventoryProduct();
        }
        invProd.setQuantity(request.getQuantity());

        inventoryProductRepository.save(invProd);
        ProductQuantityResponse response = new ProductQuantityResponse(invProd);
        return ResponseEntity.ok(response);
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody ResponseEntity getQuantity(@PathVariable Long id) {
        Inventory inventory = inventoryRepository.findById(id).get();

        List<InventoryProduct> invProds = inventoryProductRepository.findByInventoryId(inventory.getId());
        List<ProductQuantityResponse> response = new ArrayList<>();

        invProds.forEach(prodQty -> response.add(new ProductQuantityResponse(prodQty)));
        return ResponseEntity.ok(response);
    }

    @ExceptionHandler(NoSuchElementException.class)
    private  @ResponseBody ResponseEntity noSuchElementExceptionHandler() {
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

}
