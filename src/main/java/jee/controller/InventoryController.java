package jee.controller;

import jee.domain.inventory.InventoryRequest;
import jee.domain.inventory.InventoryResponse;
import jee.entity.Inventory;
import jee.entity.security.User;
import jee.entity.util.State;
import jee.entity.util.Updater;
import jee.repository.InventoryRepository;
import jee.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping(path = "/inventory")
public class InventoryController {

    private final InventoryRepository inventoryRepository;

    public InventoryController(InventoryRepository inventoryRepository, UserRepository userRepository) {
        this.inventoryRepository = inventoryRepository;
        UserRepository userRepository1 = userRepository;
    }

    @GetMapping(path = "/all")
    public @ResponseBody ResponseEntity getInventories() {
        List<InventoryResponse> response = new ArrayList<>();
        inventoryRepository.findAll()
                .forEach(inv -> response.add(new InventoryResponse(inv)));
        return ResponseEntity.ok(response);
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody ResponseEntity getInventory(@PathVariable Long id) {
        Inventory inventory = inventoryRepository.findById(id).get();
        InventoryResponse response = new InventoryResponse(inventory);
        return ResponseEntity.ok(response);
    }

    @PostMapping(path = "")
    public @ResponseBody ResponseEntity createInventory() {
        Inventory inventory = new Inventory();
        inventory.setState(State.PLANNING);
        inventory.setCreationDate(new Date());
        inventoryRepository.save(inventory);
        InventoryResponse response = new InventoryResponse(inventory);
        return ResponseEntity.ok(response);
    }

    @PutMapping(path = "/{id}")
    public @ResponseBody ResponseEntity updateInventory(@PathVariable Long id, @RequestBody InventoryRequest request) {
        Inventory inventory = inventoryRepository.findById(id).get();
        Updater.updateEntity(inventory, request);
        InventoryResponse response = new InventoryResponse(inventory);
        inventoryRepository.save(inventory);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping(path = "/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody ResponseEntity deleteInventory(@PathVariable Long id) {
        Inventory inventory = inventoryRepository.findById(id).get();
        inventoryRepository.delete(inventory);
        InventoryResponse response = new InventoryResponse(inventory);
        return ResponseEntity.ok(response);
    }

    @PostMapping(path = "/start/{id}")
    public @ResponseBody ResponseEntity startInventory(@PathVariable Long id) {
        Inventory inventory = inventoryRepository.findById(id).get();
        inventory.setState(State.DURING);
        inventoryRepository.save(inventory);
        InventoryResponse response = new InventoryResponse(inventory);
        return ResponseEntity.ok(response);
    }

    @PostMapping(path = "/close/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody ResponseEntity closeInventory(@PathVariable Long id) {
        Inventory inventory = inventoryRepository.findById(id).get();
        inventory.setState(State.CLOSED);
        inventory.setCloseDate(new Date());
        inventoryRepository.save(inventory);
        InventoryResponse response = new InventoryResponse(inventory);
        return ResponseEntity.ok(response);
    }

    @ExceptionHandler(NoSuchElementException.class)
    private @ResponseBody ResponseEntity noSuchElementExceptionHandler() {
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

}