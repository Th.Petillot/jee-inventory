package jee.entity.util;

import jee.domain.inventory.InventoryRequest;
import jee.domain.product.ProductRequest;
import jee.entity.Inventory;
import jee.entity.Product;

public class Updater {

    public static void updateEntity(Product entity, ProductRequest request) {
        entity.setName(request.getName());
    }

    public static void updateEntity(Inventory entity, InventoryRequest request) {
        entity.setState(request.getState());
        entity.setCreationDate(request.getCreationDate());
        entity.setCloseDate(request.getCloseDate());
    }

}
