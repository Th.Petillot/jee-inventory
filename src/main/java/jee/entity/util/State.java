package jee.entity.util;

public enum State {
    PLANNING, DURING, CLOSED
}
