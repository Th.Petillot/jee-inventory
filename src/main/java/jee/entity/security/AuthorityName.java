package jee.entity.security;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}
