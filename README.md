# Service REST JEE Inventory


**`/product`**

| METHODE       |       PATH      |   Body     |   Response     | Description | Authorisation |
| :------------:| --------------- | :----------:| :---------: | -----------| :-----------: |
| `GET`| /product/all | - | `Array<Product>` | Retourne l'ensemble des produits enregistrés | `Role_Admin` |
| `GET` | /product/**{id}**| - | `Product` | Retourne le produit identifié par **{id}** | `Role_Admin` |
| `PUT` | /product/**{id}**| `Product` | `Product` | Modifie le produit identifié par **{id}** | `Role_Admin` |
| `POST` | /product| `Product` | `Product` | Ajoute le produit fournis dans la corps de la requête | `Role_Admin` |
| `DELETE` | /product/**{id}**| - |`Product` |Surpprime le produit identifié par **{id}** si non utilisé dans un inventaire. | `Role_Admin` |

**`/inventory`**

| METHODE       |       PATH      |   Body     |   Response     | Description | Authorisation |
| :-----------: | --------------- | :--------: | :--------:   | ----------- | ------------- |
| `GET` | /inventory/all | - | `Array<Inventory>` |Retourne l'ensemble des inventaires | |
| `GET` | /inventory/**{id}** | - | `Inventory` |Retourne l'inventaire identifié par **{id}** | |
| `PUT` | /inventory/**{id}** | `Inventory` | `Inventory` | Modifie l'inventaire identifié par **{id}** | |
| `POST` | /inventory | Any | `Inventory` |Ajoute un inventaire non ouvert à la date du jour | |
| `POST` | /inventory/start/**{id}** | Any | `Inventory` | Ouvre l'inventaire identifié par **{id}** | |
| `POST` | /inventory/close/**{id}** | Any | `Inventory` | Ferme l'inventaire identifié par **{id}** | `Role_Admin` |
| `DELETE` | /inventory/**{id}** | - | `Inventory` | Supprime l'inventaire identifié par **{id}** | `Role_Admin` |

**`/quantity`**

| METHODE       |       PATH      |   Body     |   Response     | Description | Authorisation |
| :-----------: | --------------- | :--------: | :--------: | ----------- | ----------- |
| `GET` | /quantity/**{id}** | - | `ProductQuantity` | Récupère la quantité d'un objet dans l'inventaire identifié par **{id}** | |
| `PUT` | /quantity/add/**{id}** | `ProductQuantity` | `ProductQuantity` | Ajoute la quantité d'un objet dans l'inventaire identifié par **{id}** | |
| `PUT` | /quantity/remove/**{id}** | `ProductQuantity` | `ProductQuantity` | Remove la quantité d'un objet dans l'inventaire identifié par **{id}** | |
| `PUT` | /quantity/**{id}** | `ProductQuantity` | `ProductQuantity` | Défini la quantité d'un objet dans l'inventaire identifié par **{id}** | |

**`/user`**

| METHODE       |       PATH      |   Body     |   Response     | Description | Authorisation |
| :-----------: | --------------- | :--------: | :--------: | ----------- | ----------- |
| `GET` | /user/all | - | `Array<UserResponse>` | Retourne l'ensemble des utilisateurs | `Role_Admin` |
| `GET` | /user/**{id}** | - | `UserResponse` | Retourne l'utilisateur identifié par **{id}** | `Role_Admin` |
| `POST` | /user | `User` | `UserResponse` | Crée l'utilisateur | `Role_Admin` |
| `POST` | /user/**{id}**/role | `Array<Authorities>` | `UserResponse` | Définie les permissions de l'utilisateur identifié par **{id}** | `Role_Admin` |

**Endpoints d'authentification**

| METHODE       |       PATH      |   Body     |   Response     | Description | Authorisation |
| :-----------: | --------------- | :--------: | :--------: | ----------- | ----------- |
| `GET` | /refresh | - | `JwtAuthenticationResponse` | Rafraichissement du jeton d'authentification |  |
| `POST` | /auth | `JwtAuthenticationRequest` | `JwtAuthenticationResponse` | Demande de jeton d'authentification | |

### Object

`Product`
```json
{
  "id": 1,
  "name": "Stylo BIC"
}
```

`Inventory`
```json
{
    "id": 4,
    "state": "OPEN",
    "creationDate": "2019-01-27T20:41:15.235+0000",
    "closeDate": null
}
```

`ProductQuantity`
```json
{
    "productId": 1,
    "quantity": 5
}
```

`User`
```json
{
    "id": 1,
    "email": "jean.dupond@gmail.com",
    "password": "secret_password",
    "enable": true,
    "lastPasswordResetDate": "2019-01-27T20:41:15.235+0000"
}
```

`UserResponse`
```json
{
    "id": 3,
    "email": "jean.dupond@gmail.com",
    "Authorities": ["ROLE_ADMIN", "ROLE_USER"]
}
```

`Authorities`
```java
public enum Langage {
  ROLE_ADMIN,
  ROLE_USER;	
}
```

`JwtAuthenticationRequest`
```json
{
	"username": "admin@admin.com",
	"password": "admin"
}
```

`JwtAuthenticationResponse`
```json
{
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBhZG1pbi5jb20iLCJpYXQiOjE1NDg2MjI0ODMsImV4cCI6MTU0ODYyOTY4M30.se-B-o6fqZuq72mJvSaQs6_EtH7rMzKDYxA8puG2t4MEXeGaRXlTPleYlZKg9Ew9PVbiEUdC0mLXfktImlmsgQ",
    "authorities": [
        "ROLE_USER",
        "ROLE_ADMIN"
    ],
    "expiration": "2019-01-27T22:54:43.000+0000"
}
```